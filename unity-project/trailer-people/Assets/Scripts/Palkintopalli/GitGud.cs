﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GitGud : MonoBehaviour
{
    private float time;

    public float restartAfter = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        time = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - time > restartAfter)
        {
            if (BlackBoard.DriveGameManager)
                BlackBoard.DriveGameManager.DestroySelf();

            UnityEngine.SceneManagement.SceneManager.LoadScene("start");
        }
    }
}
