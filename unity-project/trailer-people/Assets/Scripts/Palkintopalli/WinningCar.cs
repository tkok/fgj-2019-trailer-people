﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinningCar : MonoBehaviour
{
    public int playerNumber = 0;
    public List<AudioClip> winning;
    public AudioClip music;
    private float time;

    public float restartAfter = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        if (playerNumber == 0 && null != winning)
            gameObject.AddComponent<AudioSource>().PlayOneShot(winning[Random.Range(0, winning.Count)]);

        if (playerNumber == 0 && music)
            gameObject.AddComponent<AudioSource>().PlayOneShot(music);

        GameObject kolonel = GameObject.Find("StartupKolonel");
        if (kolonel)
        {
            StartupKolonel kol = kolonel.GetComponent<StartupKolonel>();
            kol.DestroySelf();
        }

        if (!BlackBoard.DriveGameManager)
            return;

        BlackBoard.DriveGameManager.freeMove = true;
        GetComponent<SpriteRenderer>().enabled = false;

        if (playerNumber >= BlackBoard.DriveGameManager.cars.Count)
            return;

        CarController car = BlackBoard.DriveGameManager.cars[playerNumber];

        if (car && car.inUse)
        {
            car.transform.SetParent(transform);
            car.transform.localPosition = Vector3.zero;
            car.transform.localRotation = Quaternion.identity;
            car.transform.localScale = Vector3.one * 2;

            foreach (var pulsate in car.GetComponentsInChildren<Pulsate>())
            {
                pulsate.enabled = false;
            }
        }

        time = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerNumber != 0)
            return;

        if (Time.time - time > restartAfter)
        {
            if (BlackBoard.DriveGameManager)
                BlackBoard.DriveGameManager.DestroySelf();

            UnityEngine.SceneManagement.SceneManager.LoadScene("start");
        }
    }
}
