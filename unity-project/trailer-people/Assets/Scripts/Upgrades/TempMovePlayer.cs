﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempMovePlayer : MonoBehaviour
{
    private float velo = 50.0f;
    private bool noHonkPlz = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 move = Vector2.zero;
        move.y += Input.GetAxis("Vertical0");
        move.x += Input.GetAxis("Horizontal0");
        GetComponent<Rigidbody2D>().AddForce(move * velo);

        if (Input.GetAxis("Honk0") != 0)
        {
            if (!noHonkPlz)
            {
                noHonkPlz = true;
                GetComponent<AttachmentOwner>().UseAction();
            }
        }
        else
        {
            noHonkPlz = false;
        }
    }
}
