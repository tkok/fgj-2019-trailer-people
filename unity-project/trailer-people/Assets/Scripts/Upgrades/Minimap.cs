﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{

    public Transform startPos;
    public Transform endPos;

    public Transform water;
    public Transform caravan;

    private void Awake()
    {
        BlackBoard.Minimap = this;
    }

    // Update is called once per frame
    void Update()
    {
        float caravanT = BlackBoard.DriveGameManager.traveledDistance / BlackBoard.DriveGameManager.goalDistance;
        caravan.position = endPos.position * caravanT + (1 - caravanT) * startPos.position;
        float waterT = BlackBoard.DriveGameManager.waterPosition / BlackBoard.DriveGameManager.goalDistance;

        water.localScale = new Vector3((waterT * 2.15f) + 1.5f, 0.15f, 0.15f);
    }
}
