﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulsate : MonoBehaviour
{
    public AnimationCurve curvePosX;
    public AnimationCurve curvePosY;
    public AnimationCurve curveSizeX;
    public AnimationCurve curveSizeY;
    public AnimationCurve curveRot;
    public float timeMult = 1.5f;
    public float magnitudeMult = 0.1f;
    public float pulseWithSpeed = 0.0f;
    public bool noRandom = false;

    private Vector3 startPos;
    private Vector3 startSize;
    private float startAngle = 0;
    private float timeOffset = 0;
    private float timeWent = 0;

    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.localPosition;
        startSize = transform.localScale;
        startAngle = transform.localEulerAngles.z;

        if (noRandom)
            return;

        timeOffset = Random.value;
        timeMult *= Random.Range(0.90f, 1.1f);
    }

    // Update is called once per frame
    void Update()
    {
        timeWent += Time.deltaTime * timeMult;
        if (pulseWithSpeed != 0.0f)
        {
            float v = BlackBoard.ScrollManager.speed * Time.deltaTime * pulseWithSpeed;
            v = Mathf.Clamp(v, 0.0f, 3.0f);
            timeWent += v;
        }

        float t = timeWent + timeOffset;
        transform.localPosition = startPos + new Vector3(curvePosX.Evaluate(t), curvePosY.Evaluate(t)) * magnitudeMult;
        transform.localScale = startSize + new Vector3(curveSizeX.Evaluate(t), curveSizeY.Evaluate(t)) * magnitudeMult;

        if (!GetComponent<Spin>())
            transform.localEulerAngles = new Vector3(0,0, startAngle + curveRot.Evaluate(t)) * magnitudeMult;
    }
}
