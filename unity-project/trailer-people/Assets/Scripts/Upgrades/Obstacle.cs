﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public List<AttachmentKind> destructibleWith = new List<AttachmentKind>();

    private bool flyAway = false;
    private Vector2 flyVelocity;
    private float rotSpeed;

    public GameObject explosionPrefab;
    private GameObject explosion;

    // Start is called before the first frame update
    void Start()
    {
        flyVelocity = new Vector2(Random.Range(-30.0f, 30.0f), Random.Range(-30.0f, -10.0f));
        rotSpeed = Random.value < 0.5f ? Random.Range(-90.0f, -30.0f) : Random.Range(30.0f, 90.0f);

        if (explosionPrefab != null)
        {
            explosion = Instantiate<GameObject>(explosionPrefab);
            explosion.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!flyAway)
            return;

        transform.localPosition += (Vector3)flyVelocity * Time.deltaTime;
        Vector3 rot = transform.localEulerAngles;
        rot.z += Time.deltaTime * rotSpeed * 100;
        transform.localEulerAngles = rot;
    }

    internal void DestroyEffects()
    {
        if(this.explosion != null && !this.explosion.activeSelf) {
            Destroy(this.explosion);
        }
    }

    public bool canDestroy(AttachmentKind attachmentKind)
    {
        return destructibleWith.Contains(attachmentKind);
    }

    public void kaboom()
    {
        if (flyAway)
            return;

        if (explosion != null)
        {
            explosion.transform.position = transform.position;
            explosion.transform.localScale = transform.localScale;
            explosion.SetActive(true);
        }

        flyAway = true;
        foreach (ParticleSystem particleSystem in GetComponentsInChildren<ParticleSystem>())
        {
            particleSystem.Play();
        }
    }
}
