﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

public class AttachmentOwner : MonoBehaviour
{
    public List<Attachment> attachments;

    public AudioClip honkHorn;
    public AudioClip honkSuperHorn;

    private AudioSource audioSource;

    private float cooldown = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        attachments = GetComponentsInChildren<Attachment>().ToList();
        audioSource = GetComponentInParent<AudioSource>();
    }

    public void UseAction() {
        if (null != attachments.Find(a => a.attachmentKind == AttachmentKind.Horn))
        {
            if (cooldown <= 0 && audioSource)
            {
                audioSource.pitch = Random.Range(0.7f, 0.8f);
                audioSource.volume = Random.Range(1.0f, 1.3f);
                audioSource.PlayOneShot(honkSuperHorn);
                cooldown = 0.8f;
            }
        }
        else
        {
            if (cooldown <= 0 && audioSource)
            {
                audioSource.pitch = Random.Range(1.5f, 1.8f);
                audioSource.volume = Random.Range(0.7f, 1.0f);
                audioSource.PlayOneShot(honkHorn);
                cooldown = 0.4f;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        cooldown -= Time.deltaTime;
        foreach (Attachment attachment in attachments)
        {
            attachment.transform.SetParent(gameObject.transform);
            attachment.transform.localPosition = Vector3.zero + attachment.GetComponent<Attachment>().offset;
            Vector3 rot = attachment.transform.localEulerAngles;
            rot.z = attachment.GetComponent<Attachment>().angle;
            attachment.transform.localEulerAngles = rot;
        }

    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider != null)
        {
            AttachmentPickup pickup = collider.GetComponent<AttachmentPickup>();
            if (pickup != null && pickup.attachment != null && pickup.canPickup())
            {
                Attachment existingAttachment = attachments.Find((a => a.attachmentType == pickup.attachment.attachmentType));
                Attachment newAttachment = pickup.attachment;
                
                if (existingAttachment != null)
                {
                    foreach (AttachmentSounds a in existingAttachment.GetComponentsInChildren<AttachmentSounds>())
                    {
                        a.noYou();
                    }
                    existingAttachment.transform.SetParent(null);
                }

                pickup.attachment = existingAttachment;
                attachments.Remove(existingAttachment);

                attachments.Add(newAttachment);
                newAttachment.pickup();

                newAttachment.transform.SetParent(gameObject.transform);
                foreach (AttachmentSounds a in newAttachment.GetComponentsInChildren<AttachmentSounds>())
                {
                    a.gotYou();
                }
            }
        }
    }
}
