﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumperAttachment : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider == null)
            return;

        Obstacle obstacle = collider.gameObject.GetComponentInChildren<Obstacle>();
        if (obstacle != null)
        {
            AttachmentKind attachmentKind = GetComponent<Attachment>().attachmentKind;
            if (obstacle.canDestroy(attachmentKind))
            {
                obstacle.kaboom();
                if(GetComponentInParent<AudioSource>() != null)
                {

                    GetComponentInParent<AudioSource>().PlayOneShot(GetComponent<AttachmentSounds>().hitSound);
                }
            }
        }
    }
}
