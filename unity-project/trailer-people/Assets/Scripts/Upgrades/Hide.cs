﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hide : MonoBehaviour
{
    public float changeSpeed = 2.0f;

    [NonSerialized]
    public bool visible = false;
    private float alpha = 0.0f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float change = changeSpeed * Time.deltaTime * (visible ? 1.0f : -1.0f);
        alpha = Mathf.Clamp01(alpha + change);

        foreach (SpriteRenderer spriteRenderer in GetComponentsInChildren<SpriteRenderer>())
        {
            Color c = spriteRenderer.color;
            c.a = alpha;
            spriteRenderer.color = c;
        }
    }
}
