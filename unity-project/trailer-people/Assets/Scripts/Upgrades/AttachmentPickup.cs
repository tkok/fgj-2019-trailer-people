﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachmentPickup : MonoBehaviour
{
    public Attachment attachment;
    public float colliderRadius = 0.5f;
    private float spin = 1.0f;
    private float spinSpeed = 1000.0f;
    private float inactivity = 0.0f;

    void Start()
    {
        attachment = GetComponentInChildren<Attachment>();

        if (!GetComponent<Collider2D>())
        {
            var collider = gameObject.AddComponent<CircleCollider2D>();
            collider.radius = colliderRadius;
            collider.isTrigger = true;
        }
    }

    void Update()
    {
        if (inactivity > 0)
            inactivity -= Time.deltaTime;

        if (attachment != null)
        {
            GameObject attachmentObject = attachment.gameObject;
            if (attachmentObject == null)
                return;

            if (Mathf.Abs(spin) > 0.01f)
            {
                Vector3 rot = attachmentObject.transform.localEulerAngles;
                rot.z += spin * spinSpeed * Time.deltaTime;
                attachmentObject.transform.localEulerAngles = rot;
                spin = Mathf.Lerp(spin, 0, 0.01f * Time.deltaTime * 60.0f);
            }

            if (attachmentObject.transform.parent != gameObject.transform)
            {
                attachmentObject.transform.SetParent(gameObject.transform);
                attachmentObject.transform.localPosition = Vector3.zero;
                spin = Random.value < 0.5f ? Random.Range(-1.0f, -0.4f) : Random.Range(0.4f, 1.0f);
                attachment.drop();
                inactivity = 0.5f;
            }
        }
    }

    public bool canPickup()
    {
        return inactivity <= 0.0f;
    }
}
