﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttachmentType
{
    Bumper,
    Hood,
}

public enum AttachmentKind
{
    CircularSaw,
    Plow,
    Cushion,
    FireHose,
    Horn,
    Propeller,
    Tool,
    Lamp,
    Damage,
}

public class Attachment : MonoBehaviour
{
    public Vector3 offset = Vector3.zero;
    public float angle = 0.0f;
    public AttachmentType attachmentType;
    public AttachmentKind attachmentKind;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void drop()
    {
        foreach (Spin s in GetComponentsInChildren<Spin>())
        {
            s.enabled = false;
        }
        foreach (Hide s in GetComponentsInChildren<Hide>())
        {
            s.visible = false;
        }
    }

    public void pickup()
    {
        foreach (Spin s in GetComponentsInChildren<Spin>())
        {
            s.enabled = true;
        }
        foreach (Hide s in GetComponentsInChildren<Hide>())
        {
            s.visible = true;
        }
    }
}
