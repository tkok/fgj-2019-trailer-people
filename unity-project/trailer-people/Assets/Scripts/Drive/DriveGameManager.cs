﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DriveGameManager : MonoBehaviour
{
    public List<CarController> cars = new List<CarController>();

    public float traveledDistance = 0;
    public float goalDistance = 1000;

    public AudioClip slowMusic;
    public AudioClip normalMusic;
    public AudioClip fastMusic;
    private AudioSource audioSource;

    public float waterPosition = -500;
    private float waterSpeed = 10;
    private float waterAcceleration = 10;
    private float waterMaxSpeed = 100;
    private DriveLevelData currentLevel;
    public bool freeMove = true;

    public string currentPrize;

    public List<EventData> events;
    private EventData currentEvent;
    public List<DriveLevelData> levels;
    public PainShop startPaint;

        public int driveCount = 0;
    private bool lost = false;

    internal int GetCarCount()
    {
        int result = 0;
        for(int i= 0; i < cars.Count; ++i)
        {
            if (this.cars[i].inUse) {
                result++;
            }
        }
        return result;
    }

    internal void ToEvent()
    {
        //save water progression
        waterPosition = waterPosition - goalDistance;
        waterPosition -= 50;
        waterPosition = Mathf.Clamp(waterPosition, -200, -20);

        waterSpeed = 0;


        this.audioSource.Stop();
        freeMove = true;
        UnityEngine.SceneManagement.SceneManager.LoadScene("event");
        for (int i = 0; i < cars.Count; ++i)
        {
            if (cars[i].inUse)
            {
                cars[i].GoToStartPos();
            }
        }
    }

    internal int GetHorns(float hornMaxTime, List<CarController> colliders)
    {
        int result = 0;
        for(int i = 0; i < cars.Count; ++i) { 
            if(cars[i].lastHornTime > 0 && Time.time < cars[i].lastHornTime + hornMaxTime && colliders.Contains(cars[i]))
            {
                result++;
            }
        }
        return result;
    }

    internal void StartNextLevel(int driveLevelDataIndex)
    {
        Debug.Assert(driveLevelDataIndex < levels.Count);
        driveCount++;
        this.StartDriveLevel(this.levels[driveLevelDataIndex]);
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        BlackBoard.DriveGameManager = this;
        DontDestroyOnLoad(gameObject);
        BlackBoard.DriveGameManager = this;
        this.cars.Clear();
        this.cars = new List<CarController>(FindObjectsOfType<CarController>());
        this.audioSource = gameObject.AddComponent<AudioSource>();
        this.audioSource.volume = 0.25f;
        for(int i = 0; i < cars.Count; ++i)
        {
            cars[i].playerNo = i;
            cars[i].ChangeColor(startPaint.paintShops[i], startPaint.paintShops[i]);
        }
    }

    internal void GoToPrizeScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("collectprize");
        freeMove = true;

        for (int i = 0; i < cars.Count; ++i)
        {
            if (cars[i].inUse)
            {
                cars[i].GoToStartPos();
            }
        }
    }

    private void LateUpdate()
    {
        if (!freeMove && BlackBoard.SpeedLine != null)
        {
            //update by my position from line
            float speed = 0;
            int activeCars = 0;
            float jakaja = 0;
            for (int i = 0; i < cars.Count; ++i)
            {
                if (!cars[i].inUse) continue;
                activeCars++;
                jakaja += 1 / activeCars;
                float t = (transform.position.y - BlackBoard.SpeedLine.y) / (BlackBoard.MaxLine.y - BlackBoard.SpeedLine.y);
                float carSpeed = BlackBoard.SpeedLine.y - cars[i].transform.position.y;

                if(carSpeed < 0)
                {
                    carSpeed = carSpeed * carSpeed;
                }
                else {
                    carSpeed = -carSpeed;
                }

                speed += 0.5f * carSpeed * (1 + t);
            }

            BlackBoard.ScrollManager.SetSpeed((speed / activeCars) * jakaja);

            traveledDistance += Time.deltaTime * speed;
            waterPosition += Time.deltaTime * waterSpeed;

            waterSpeed += waterAcceleration * Time.deltaTime;
            waterSpeed = Mathf.Clamp(waterSpeed, 0, waterMaxSpeed);
        }
    }

    internal void SetActiveCarsAndReset(List<CarController> carsOnBox)
    {
        traveledDistance = 0;
        waterSpeed = 0;
        waterPosition = -200;
        waterAcceleration = 1;
        waterMaxSpeed = 100;
        lost = false;

        for(int i =0; i < cars.Count; ++i) {
            cars[i].SetActive(carsOnBox.Contains(cars[i]));
        }
    }

    internal void StartDriveLevel(DriveLevelData data)
    {
        
        this.goalDistance = data.distance;
        this.traveledDistance = 0;
        this.currentLevel = data;
        UnityEngine.SceneManagement.SceneManager.LoadScene("drive");
        this.freeMove = false;

        //Music
        this.audioSource.clip = slowMusic;
        if(driveCount > 1) {
            this.audioSource.clip = normalMusic;
        }
        if(driveCount > 3) {
            this.audioSource.clip = fastMusic;
        }
        this.audioSource.Play();

        waterAcceleration = 0.75f + 0.5f * driveCount;

        currentEvent = this.events[Random.Range(0, this.events.Count)];


        for (int i = 0; i < cars.Count; ++i)
        {
            if (cars[i].inUse) {
                cars[i].GoToStartPos();
            }
        }
    }

    internal EventData GetCurrentEvent()
    { 
        return currentEvent;
    }

    internal void Lose()
    {
        this.audioSource.Stop();
        if (!lost && !freeMove)
        {
            foreach (CarController car in cars)
            {
                foreach (Attachment attachment in car.GetComponentsInChildren<Attachment>())
                {
                    attachment.gameObject.SetActive(false);
                }
                Destroy(car.gameObject);
            }
            cars.Clear();
            freeMove = true;
            GameObject startupKolonel = GameObject.Find("StartupKolonel");
            if (startupKolonel)
                startupKolonel.GetComponent<StartupKolonel>().DestroySelf();

            UnityEngine.SceneManagement.SceneManager.LoadScene("now_you_die_git_gud");
            lost = true;
        }
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }
}
