﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterVisual : MonoBehaviour
{
    private float startY;
    void Start()
    {
        startY = transform.localPosition.y;
    }


    // Update is called once per frame
    void Update()
    {
        float y = BlackBoard.DriveGameManager.waterPosition - BlackBoard.DriveGameManager.traveledDistance;
        transform.localPosition = new Vector3(0, startY + y * 0.05f, 0);
    }
}
