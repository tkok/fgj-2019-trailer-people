﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stuff : MonoBehaviour
{
    public Rigidbody2D rb;
    public SpriteRenderer sr;

    public bool endStuff = false;
    private bool gameEnded = false;
    public float speedMultiplier = 1;

    private void Start()
    {
        if (this.endStuff)
        {
            this.sr.sprite = BlackBoard.DriveGameManager.GetCurrentEvent().backgroundSprite;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!BlackBoard.DriveGameManager.freeMove)
        {
            rb.velocity = new Vector2(0, -BlackBoard.ScrollManager.speed * speedMultiplier);
            if (endStuff || BlackBoard.KillLine == null) return;
            if (transform.position.y < BlackBoard.KillLine.transform.position.y)
            {
                Obstacle obstacle = GetComponent<Obstacle>();
                if (obstacle) {
                    obstacle.DestroyEffects();
                }
                Destroy(gameObject);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (endStuff)
        {
            if (!gameEnded && collision.collider.GetComponent<CarController>())
            {
                BlackBoard.StartMenuCamera.StartGameEffect();
                Invoke("LoadEventScene", 1);
                gameEnded = true;

            }
        }
    }

    public void LoadEventScene()
    {
        CancelInvoke("LoadEventScene");
        BlackBoard.DriveGameManager.ToEvent();
    }
}
