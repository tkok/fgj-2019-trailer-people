﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSpawn : MonoBehaviour
{
    public GameObject endPrefab;
    private bool endSpawned = false;

    private void Update()
    {
        if(!endSpawned && BlackBoard.DriveGameManager.traveledDistance > BlackBoard.DriveGameManager.goalDistance) {
            Instantiate<GameObject>(endPrefab, transform.position, Quaternion.identity);
            endSpawned = true;
        }
    }

}
