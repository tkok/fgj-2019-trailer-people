﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollManager : MonoBehaviour
{

    public float speed = 0;
    public float maxSpeed = 10;
    public float minSpeed = 0;

    private void Awake()
    {
        BlackBoard.ScrollManager = this;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        this.speed = Mathf.Clamp(speed, minSpeed, maxSpeed);
    }

    internal void SetSpeed(float newSpeed)
    {
        this.speed = Mathf.Clamp(newSpeed, this.minSpeed, this.maxSpeed);
    }
}
