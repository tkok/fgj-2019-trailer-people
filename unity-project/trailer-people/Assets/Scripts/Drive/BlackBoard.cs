﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BlackBoard
{
    public static ScrollManager ScrollManager;
    public static SpeedLine SpeedLine;
    public static KillLine KillLine;
    public static MaxLine MaxLine;
    public static MinLine MinLine;
    public static Minimap Minimap;
    public static DriveGameManager DriveGameManager;
    public static StartMenuCamera StartMenuCamera;
	public static MapManager mapManager;
}
