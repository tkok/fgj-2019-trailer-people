﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotation : MonoBehaviour
{
    float randomSize = 1.2f;
    // Start is called before the first frame update
    void Start()
    {
        transform.localEulerAngles = new Vector3(0, 0, Random.Range(0, 360));
        transform.localScale = Vector3.one * (1f + randomSize * Random.value);
    }
}
