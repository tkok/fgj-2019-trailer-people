﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaxLine : MonoBehaviour
{
    public float y = 0;
    // Start is called before the first frame update
    void Awake()
    {
        BlackBoard.MaxLine = this;
    }

    private void Update()
    {
        this.y = transform.position.y;
    }

}
