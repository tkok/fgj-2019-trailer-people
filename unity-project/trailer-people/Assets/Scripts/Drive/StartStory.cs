﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartStory : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (BlackBoard.DriveGameManager.driveCount != 0) {
            Destroy(gameObject);
        }
    }

}
