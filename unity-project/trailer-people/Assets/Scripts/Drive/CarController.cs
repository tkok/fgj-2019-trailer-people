﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{

    public Vector2 mySpeed = Vector2.zero;

    public Rigidbody2D rb;
    public float horizontalAcceleration = 10;
    public float horizontalMaxSpeed = 10;
    public float horizontalDamp = 0.5f;

    public float verticalAcceleration = 10;
    public float verticalMaxSpeed = 10;

    public float oikaisuvoima = 10; // goto zero degrees
    public float rotationInputMultiplier = 10;

    public float angularDamp = 0.25f;
    public float speedJakajaAngluarKorjaimeen = 100;

    public int playerNo = 0;

    public float lastHornTime = 0;


    public float targetAngle = 0;
    public float maxAngle = 45;
    public float minAngle = 315;

    public bool inUse = false;
    private Vector3 startPos;

    private AttachmentOwner attachmentOwner;

    public SpriteRenderer paint;

    private AudioSource audioSource;
    public AudioClip collisionAudioClip;

    internal void ChangeColor(Sprite sprite, Sprite secondary)
    {
        if (UnityEngine.Random.value > 0.95f)
        {
            if(UnityEngine.Random.value > 0.6f)
            {
                this.paint.color = new Color(0.05f, 0.05f, 0.05f, 1f);

            }
            else
            {
                this.paint.color = new Color(1, 1, 1, 0.5f);
            }
        }
        else
        {
            this.paint.color = new Color(1, 1, 1, 1);
        }
        if (this.paint.sprite != sprite)
        {
            this.paint.sprite = sprite;

        }
        else
        {
            this.paint.sprite = secondary;
        }
    }


    private void Awake()
    {
        startPos = transform.position;
        DontDestroyOnLoad(gameObject);
        this.attachmentOwner = GetComponent<AttachmentOwner>();
        this.audioSource = gameObject.AddComponent<AudioSource>();
    }


    void FixedUpdate()
    {


        //MOVE & HORN
        float verticalInput = 0;
        float horizontalInput = 0;
        float horn = 0;

        // Input.GetAxis("Vertical" + playerNo) + ", " + Input.GetAxis("Honk" + playerNo));
        horizontalInput = Input.GetAxis("Horizontal" + playerNo);
        verticalInput = (Input.GetAxis("Vertical" + playerNo));
        horn = Input.GetAxis("Honk" + playerNo);

        if (horn > 0) {
            lastHornTime = Time.time;
            this.attachmentOwner.UseAction();
        }


        if (!BlackBoard.DriveGameManager.freeMove)
        {
            if (transform.position.y > BlackBoard.MaxLine.transform.position.y)
            {
                Vector3 v3 = transform.position;
                v3.y = BlackBoard.MaxLine.transform.position.y;
                transform.position = v3;
                mySpeed = new Vector2(mySpeed.x, 0);
            }

            if (transform.position.y < BlackBoard.MinLine.transform.position.y)
            {
                Vector3 v3 = transform.position;
                v3.y = BlackBoard.MinLine.transform.position.y;
                transform.position = v3;
                mySpeed = new Vector2(mySpeed.x, 0);
            }
        }
        //reduce velosity dependin on distance to max and min line:
        //float t = (transform.position.y - BlackBoard.SpeedLine.y) / (BlackBoard.MaxLine.transform.position.y - BlackBoard.SpeedLine.y);
        //mySpeed = new Vector2(mySpeed.x, mySpeed.y * t);


        //ANGLE
        float angle = transform.eulerAngles.z % 360;


        float anglularForce = 0; //rotationInput * rotationInputMultiplier;
        // rotate toward 0
        if(angle > 180) {
            anglularForce += ((angle - 360) / 45);
        }
        else {
            anglularForce += (angle / 45);
        }
        float korjaus = anglularForce * oikaisuvoima * Time.deltaTime * (Mathf.Max(BlackBoard.ScrollManager.speed,2) / this.speedJakajaAngluarKorjaimeen);
        // print("angel " + angle + " korjaus " + korjaus);
        if(korjaus > 0 && korjaus > angle)
        {
            korjaus = angle;
        }
        if(korjaus < 0 && angle - korjaus > 360)
        {
            korjaus = angle - 360;
        }

        angle = angle - korjaus;

        angle = angle % 360;

        // add horizontal input
        float horizontalRotation = -horizontalInput * 250 * Time.deltaTime;
        angle += horizontalRotation;
        angle = angle % 360;

        // limit
        if (angle > 180 && angle < minAngle)
        {
            transform.eulerAngles = new Vector3(0, 0, minAngle);
        }
        else if(angle < 180 && angle > maxAngle) 
        {
            transform.eulerAngles = new Vector3(0, 0, maxAngle);
        }
        else
        {
            transform.eulerAngles = new Vector3(0, 0, angle);
        }


        //Add position force // Mathf.Sin(Mathf.Deg2Rad * -angle) * this.horizontalAcceleration
        float speedX = Mathf.Clamp(mySpeed.x * (1 - this.horizontalDamp * Time.deltaTime) + horizontalInput * horizontalAcceleration * Time.deltaTime, -horizontalMaxSpeed, horizontalMaxSpeed);
        float speedY = Mathf.Clamp(mySpeed.y + Time.deltaTime * this.verticalAcceleration * verticalInput, -verticalMaxSpeed, verticalMaxSpeed);
        float locationDrag = BlackBoard.DriveGameManager.freeMove ? 0 : Mathf.Sqrt( Mathf.Max(0, (transform.position.y - BlackBoard.SpeedLine.y) / (BlackBoard.MaxLine.y - BlackBoard.SpeedLine.y)));

        locationDrag = Mathf.Clamp01(locationDrag);
        //print("Vertical drag " + locationDrag);
        if (speedY > 0)
        {
            speedY = speedY * (1 - locationDrag);
        }
        if (BlackBoard.DriveGameManager.freeMove) {
            speedY *= (1 - this.horizontalDamp * Time.deltaTime);
        }

        mySpeed = new Vector2(
            speedX,
            speedY);

        rb.velocity = mySpeed;
        rb.angularVelocity = rb.angularVelocity * (1 - angularDamp * Time.deltaTime); //- anglularForce * Time.deltaTime;
    
    }

    internal void GoToStartPos()
    {
        transform.position = startPos;
        rb.velocity = Vector2.zero;
        rb.angularVelocity = 0;
        transform.eulerAngles = Vector3.zero;
    }

    public void SetActive(bool state)
    {
        inUse = state;
        gameObject.SetActive(state);
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == 4) {
            //Debug.Log("WATER!!");
            Invoke("Lose", 1);
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        this.audioSource.pitch = Random.Range(0.5f, 1.5f);
        this.audioSource.PlayOneShot(collisionAudioClip, 1);
    }

    public void Lose() {
        BlackBoard.DriveGameManager.Lose();
    }
}
