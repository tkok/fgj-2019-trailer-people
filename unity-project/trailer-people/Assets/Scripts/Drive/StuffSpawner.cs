﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StuffSpawner : MonoBehaviour
{

    public float startInterval = 20;
    public float endInterval = 5;
    private float interval = 0;
    public float rockTreeRatio = 0.25f;

    public Vector3 randomPos;

    public float randomInterval = 5;
    public float lastSpawn;
    public GameObject[] treePrefab;
    public GameObject[] rockPrefab;
    public GameObject[] specialPrefab;

    private float lastStuffDistance = 0;

    private void Start()
    {
        float t = BlackBoard.DriveGameManager.traveledDistance / BlackBoard.DriveGameManager.goalDistance;
        interval = t * startInterval + (1 - t) * endInterval + 2 * (Random.value - 0.5f) * randomInterval;
        interval = Mathf.Clamp(interval, endInterval * 0.5f, startInterval * 2);
    }

    // Update is called once per frame
    void Update()
    {
        if(BlackBoard.DriveGameManager.traveledDistance - lastStuffDistance > interval) 
        {

            GameObject prefab = Random.value > this.rockTreeRatio ? rockPrefab[Random.Range(0, rockPrefab.Length)] : treePrefab[Random.Range(0, treePrefab.Length)];
            if(Random.value < 0.1f) {
                prefab = specialPrefab[Random.Range(0, specialPrefab.Length)];
            }
            lastStuffDistance = BlackBoard.DriveGameManager.traveledDistance;

            float t = BlackBoard.DriveGameManager.traveledDistance / 2000f; // BlackBoard.DriveGameManager.goalDistance;

            interval = t * endInterval + (1 - t) * startInterval + 2 * (Random.value - 0.5f) * randomInterval;
            interval = Mathf.Clamp(interval, endInterval * 0.5f, startInterval*2);

            if (prefab != null) 
            {
                Instantiate<GameObject>(prefab,
                    transform.position + randomPos * Random.value, Quaternion.identity);// .transform.localScale;// = Vector3.one *(1+ Random.value);
            }
            else {
                interval = interval * 0.5f;
            }

        }
    }
}
