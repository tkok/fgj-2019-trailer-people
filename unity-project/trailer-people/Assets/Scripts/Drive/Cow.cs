﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cow : MonoBehaviour
{
    public AudioClip audioClip;

    public AudioSource source;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        source.PlayOneShot(audioClip);
    }
}
