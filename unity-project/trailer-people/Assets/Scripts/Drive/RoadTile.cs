﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadTile : MonoBehaviour
{
    public Rigidbody2D rb;
    public float tileInterval = 16;
    public int index = 0;

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(0, -BlackBoard.ScrollManager.speed);
        if (transform.position.y < -tileInterval * index) {
            transform.position = new Vector3(transform.position.x, transform.position.y + tileInterval);
        }

    }
}
