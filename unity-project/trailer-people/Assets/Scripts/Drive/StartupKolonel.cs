﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartupKolonel : MonoBehaviour
{
    public AudioClip carStart;
    public AudioClip carNoise;
    private AudioSource audioSource;
    private bool running = false;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.PlayOneShot(carStart);
        audioSource.clip = carNoise;
    }

    // Update is called once per frame
    void Update()
    {
        if (!audioSource.isPlaying)
        {
            audioSource.clip = carNoise;
            audioSource.Play();
            running = true;
        }

        if (running)
        {
            float startLoud = Mathf.Clamp01(4.0f - Time.time);

            audioSource.volume = Mathf.Lerp(0.06f, 0.3f, BlackBoard.ScrollManager.speed / 60 + startLoud);
            audioSource.pitch = Mathf.Lerp(0.6f, 2.0f, BlackBoard.ScrollManager.speed / 60);
        }
    }

    public void DestroySelf()
    {
        if (audioSource)
        {
            audioSource.Stop();
        }
        Destroy(gameObject);
    }
}
