﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttachmentSounds : MonoBehaviour
{
    public AudioClip hitSound;
    public AudioClip runningClip;
    public float volume = 0.5f;

    // Start is called before the first frame update
    void Start()
    {

    }
    // Update is called once per frame
    void LateUpdate()
    {
        AudioSource a = GetComponentInParent<AudioSource>();
        if (a != null && !a.isPlaying && runningClip != null)
        {
            a.pitch = 1.0f;
            a.volume = volume;
            a.clip = runningClip;
            a.Play();
        }
    }

    public void gotYou()
    {
        AudioSource a = GetComponentInParent<AudioSource>();
        if (a != null)
        {
            a.volume = 1.0f;
            a.pitch = 1.0f;
            a.PlayOneShot(hitSound);
        }
    }

    public void noYou()
    {
        AudioSource a = GetComponentInParent<AudioSource>();
        if (a != null)
        {
            a.volume = 1.0f;
            a.pitch = 1.0f;
            a.Stop();
        }
    }


}
