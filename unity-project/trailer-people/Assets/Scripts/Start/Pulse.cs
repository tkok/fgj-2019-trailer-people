﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour
{

    Vector3 startSize;
    // Start is called before the first frame update
    void Start()
    {
        startSize = transform.localScale;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale = new Vector3(startSize.x * (0.9f+0.2f* Mathf.Sin(Time.time)), startSize.y * (0.9f + 0.2f*Mathf.Cos(Time.time)), startSize.z);
    }
}
