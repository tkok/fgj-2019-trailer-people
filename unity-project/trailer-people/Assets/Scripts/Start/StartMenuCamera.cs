﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuCamera : MonoBehaviour
{

    private bool startEffectOn = false;

    private float startTime;

    private void Awake()
    {
        BlackBoard.StartMenuCamera = this;
    }
    // Update is called once per frame
    void Update()
    {
        if (startEffectOn)
        {
            Vector3 pos = transform.position;
            pos.y = pos.y + ((Time.time - startTime) * 30 + 0.5f) * Time.deltaTime;
            transform.position = pos;
        }

    }

    internal void StartGameEffect()
    {
        if (!this.startEffectOn)
        {
            this.startEffectOn = true;
            startTime = Time.time;

        }
    }
}
