﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBox : MonoBehaviour
{
    public TMPro.TextMeshPro textMesh;
    public int carsOn = 0;
    public int hornCount = 0;
    public float hornMaxTime = 2;

    List<CarController> carsOnBox = new List<CarController>();

    public StartMenuCamera menuCamera;

    public bool gameIsStarting = false;


    // Update is called once per frame
    void Update()
    {
        if(carsOn > 0)
        {
            hornCount = BlackBoard.DriveGameManager.GetHorns(hornMaxTime, carsOnBox);
            textMesh.text = "Honk to start " + this.carsOn.ToString() + "p game\n(space/F/gamepad A/X/etc)";
            if(carsOn == hornCount) {
                StartNewGame();
            }
            else if (hornCount > 0) {
                textMesh.text = "" + (carsOn - hornCount) + " more honks!\n(space/F/gamepad A/X/etc)";
            }
        }
        else {
            textMesh.text = "Start by driving here";
        }
    }

    private void StartNewGame() {
        if (!gameIsStarting) {
            Debug.Log("new game");
            this.menuCamera.StartGameEffect();
            gameIsStarting = true;
            Invoke("LoadNextScene", 2);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameIsStarting)
        {
            return;
        }
        CarController car = collision.gameObject.GetComponent<CarController>();
        if (car != null && !carsOnBox.Contains(car)){
            carsOnBox.Add(car);
            carsOn = carsOnBox.Count;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameIsStarting)
        {
            return;
        }
        CarController car = collision.gameObject.GetComponent<CarController>();
        if (car != null && carsOnBox.Contains(car)) {
            carsOnBox.Remove(car);
            carsOn = carsOnBox.Count;
        }
    }

    private void LoadNextScene() {
        CancelInvoke("LoadNextScene");
        BlackBoard.DriveGameManager.SetActiveCarsAndReset(this.carsOnBox);
        DriveLevelData data = new DriveLevelData() { snow = false, forestDensity = 0, distance = 300 };
        BlackBoard.DriveGameManager.StartDriveLevel(data);
    }

}

[System.Serializable]
public struct DriveLevelData
{
    public bool snow;
    public float forestDensity;
    public float distance;
}