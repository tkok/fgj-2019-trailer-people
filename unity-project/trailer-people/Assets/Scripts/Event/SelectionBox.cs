﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionBox : MonoBehaviour
{
    public TMPro.TextMeshPro answerLabel;
    public TMPro.TextMeshPro infoLabel;
    public int carsOn = 0;
    public int hornCount = 0;
    public float hornMaxTime = 2;
    public string selectionText;
    List<CarController> carsOnBox = new List<CarController>();

    public SelectionMaster master;

    public void Init(string text)
    {
        this.answerLabel.text = text;
        this.selectionText = text;
    }

    // Update is called once per frame
    void Update()
    {
        int activeCars = BlackBoard.DriveGameManager.GetCarCount();
        if(carsOn > 0)
        {
            hornCount = BlackBoard.DriveGameManager.GetHorns(hornMaxTime, carsOnBox);
            infoLabel.text = "Honk to select";
            if(activeCars == hornCount) {
                master.Select(this);
            }
            else if (hornCount > 0) {
                infoLabel.text = "" + (activeCars - hornCount) + " more honks!";
            }
        }
        else {
            infoLabel.text = "Drive here";
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        CarController car;
        if((car = collision.GetComponent<CarController>()) != null)
        {
            if (!carsOnBox.Contains(car))
            {
                carsOnBox.Add(car);
                carsOn = carsOnBox.Count;
                car.lastHornTime = 0;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        CarController car;
        if ((car = collision.GetComponent<CarController>()) != null)
        {
            if (carsOnBox.Contains(car))
            {
                carsOnBox.Remove(car);
                carsOn = carsOnBox.Count;
                car.lastHornTime = 0;
            }
        }
    }
}