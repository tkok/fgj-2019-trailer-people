﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PainShop : MonoBehaviour
{

    public List<Sprite> paintShops = new List<Sprite>();
    public AudioClip soundEffect;
    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Debug.Log("OnCollisionEnter2D");
        CarController car = collision.gameObject.GetComponent<CarController>();
        if (car != null)
        {
            int randomIndex = Random.Range(0, this.paintShops.Count);
            
            car.ChangeColor(paintShops[randomIndex], paintShops[(randomIndex +1)% paintShops.Count]);
            audioSource.PlayOneShot(this.soundEffect);
        }

    }
}
