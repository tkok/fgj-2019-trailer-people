﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToDrive : MonoBehaviour
{
    Vector3 pos;
    public float yAmount = 5;
    public PrizeMaster master;

    // Start is called before the first frame update
    void Start()
    {
        pos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = pos + new Vector3(0, Mathf.Sin(Time.time) * yAmount);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<CarController>() != null) {
            master.Arrow();
        }
    }
}
