﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionMaster : MonoBehaviour
{
    public TMPro.TextMeshPro label;
    public TMPro.TextMeshPro titleLabel;
    public TMPro.TextMeshPro timeLabel;

    public SelectionBox box1;
    public SelectionBox box2;
    private EventData currentData;
    private float selectionTime;
    private bool selected = false;
    public SpriteRenderer backround;

    private void Start()
    {
        currentData = BlackBoard.DriveGameManager.GetCurrentEvent();
        box1.Init(currentData.answer1);
        box2.Init(currentData.answer2);
        backround.sprite = currentData.backgroundSprite;
        titleLabel.text = currentData.title;
        label.text = currentData.question;
        if (currentData.music != null) 
        {
            GetComponent<AudioSource>().clip = currentData.music;
            GetComponent<AudioSource>().Play();
        }
    }

    private void Update()
    {
        if (this.selected) {
            this.timeLabel.text = "" + Mathf.RoundToInt(5 - (Time.time - this.selectionTime));
        }
    }


    internal void Select(SelectionBox selectionBox)
    {
        if (selected) return;

        string selection = selectionBox.selectionText;
        if(selection == currentData.answer1) {
            BlackBoard.DriveGameManager.currentPrize = this.currentData.prize1;
            this.label.text = this.currentData.feedback1;
        }
        else {
            BlackBoard.DriveGameManager.currentPrize = this.currentData.prize2;
            this.label.text = this.currentData.feedback2;
        }

        Destroy(box1.transform.parent.gameObject); // Hyi, parentti vaara
        Destroy(box2.transform.parent.gameObject); // Hyi, parentti vaara
        this.selectionTime = Time.time;
        this.selected = true;
        Invoke("CollectPrize", 5);

    }

    private void CollectPrize() {
        CancelInvoke("CollectPrize");
        BlackBoard.DriveGameManager.GoToPrizeScene();
    }
}



[System.Serializable]
public struct EventData
{
    public string title;
    public string question;
    public string answer1;
    public string answer2;
    public string feedback1;
    public string feedback2;
    public string prize1;
    public string prize2;
    public Sprite backgroundSprite;
    public AudioClip music;
}