﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrizeMaster : MonoBehaviour
{

    public GameObject circceli;
    public GameObject plow;
    public GameObject propeller;
    public GameObject superHorn;
    public GameObject headlamp;
    public GameObject tool;
    public GameObject cushion;
    public GameObject damage;
    public float startTime;
    private float timeLimit = 15;

    private bool nextLevelLoading = false;

    public TMPro.TextMeshPro timeLabel;


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Prizes: " + BlackBoard.DriveGameManager.currentPrize);
        this.circceli.SetActive(BlackBoard.DriveGameManager.currentPrize.Contains("circceli"));
        this.plow.SetActive(BlackBoard.DriveGameManager.currentPrize.Contains("plow"));
        this.propeller.SetActive(BlackBoard.DriveGameManager.currentPrize.Contains("propeller"));
        this.superHorn.SetActive(BlackBoard.DriveGameManager.currentPrize.Contains("superhorn"));
        this.headlamp.SetActive(BlackBoard.DriveGameManager.currentPrize.Contains("headlamp"));
        this.tool.SetActive(BlackBoard.DriveGameManager.currentPrize.Contains("korjaussarja"));
        this.cushion.SetActive(BlackBoard.DriveGameManager.currentPrize.Contains("tyyny"));
        this.damage.SetActive(BlackBoard.DriveGameManager.currentPrize.Contains("damage"));
        startTime = Time.time;
    }


    // Update is called once per frame
    void Update()
    {
        int timeLeft = Mathf.RoundToInt(timeLimit - Time.time + startTime);
        if (timeLeft > 0)
        {
            timeLabel.text = "Your journey continues in " + timeLeft + " seconds";
        }
        else
        {
            if (!nextLevelLoading)
            {
                BlackBoard.StartMenuCamera.StartGameEffect();
                Invoke("NextLevel", 1);
                nextLevelLoading = true;
            }
        }
    }

    public void Arrow()
    {
        if (!nextLevelLoading)
        {
            BlackBoard.StartMenuCamera.StartGameEffect();
            Invoke("NextLevel", 1);
            nextLevelLoading = true;
        
        }
    }

    private void NextLevel() {
        CancelInvoke();
        BlackBoard.mapManager.finishedDrive();
    }
}

