﻿Shader "Unlit/MapVisualShader"
{
Properties
{
	_MainTex ("Height texture", 2D) = "white" {}
	_ColorTex ("Color texture", 2D) = "white" {}
	_WaterHeight ("Water height", Range (0, 1)) = 0
	_WaterColor ("Water color", Color) = (0,0,1,1)
}
SubShader
{
	Tags { "RenderType"="Opaque" }
	LOD 100

	Pass
	{
		CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag

		#include "UnityCG.cginc"

		struct appdata
		{
			float4 vertex : POSITION;
			float2 uv : TEXCOORD0;
		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
			float4 vertex : SV_POSITION;
		};

		sampler2D _MainTex;
		sampler2D _ColorTex;
		float4 _MainTex_ST;
		float _WaterHeight;
		float4 _WaterColor;

		v2f vert (appdata v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.uv = TRANSFORM_TEX(v.uv, _MainTex);
			return o;
		}

		float4 frag (v2f i) : SV_Target
		{
			// sample the texture
			float4 heightIn = tex2D(_MainTex, i.uv);
			float terrainHeight = heightIn.x;
			
			float4 colorIn = tex2D(_ColorTex, i.uv);
			
			float4 colorOut = float4(0,0,0,1);
			float waterDetph = _WaterHeight - terrainHeight;
			
			if (waterDetph > 0 || terrainHeight < 0.001)
			{
				float plasma = (cos(i.uv.x * 8.0 + _Time.y) + sin(i.uv.y * -8.0 + _Time.y) + 2.0) / 4.0;
				plasma += cos(i.uv.y * 89 - _Time.y * 6.7) / 10;
				plasma += sin(i.uv.y * 74 - _Time.y * 8.9) / 10;
				//plasma += (sin(i.uv.x * 200 + _Time.y) + cos(i.uv.x * 230 + _Time.y)) / 20;
				
				colorOut = _WaterColor; //lerp(colorIn, _WaterColor, waterDetph);
				colorOut *= 0.2  + (plasma / 2);
				colorOut.rg += max(0, plasma - 1.0);
			}
			else
			{
				colorOut = colorIn;
			}

			return colorOut;
		}
		ENDCG
	}
}
}
