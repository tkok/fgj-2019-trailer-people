﻿using System.Collections.Generic;
using UnityEngine;

static class MapUtils
{
	public static void addPoitsuToMap(Poitsu poitsu, Transform mapTrans, float mapSize, Material material)
	{
		GameObject go = GameObject.CreatePrimitive(PrimitiveType.Quad);
		go.transform.SetParent(mapTrans);
		go.transform.localPosition = new Vector3(poitsu.worldPos.x, poitsu.worldPos.y, 0) * mapSize;
		go.transform.localRotation = Quaternion.identity;
		go.transform.localScale = Vector3.one * 5;

		go.GetComponent<MeshRenderer>().material = material;

		GameObject textGo = new GameObject();
		textGo.transform.SetParent(go.transform, false);

		TextMesh text = textGo.AddComponent<TextMesh>();
		text.text = poitsu.name;
		text.fontSize = 24;
		text.characterSize = 0.125f;
	}

	public static void addConnectionToMap(PoitsuConnection connection, MapData mapData, Transform mapTrans, float mapSize, Material material)
	{
		GameObject go = GameObject.CreatePrimitive(PrimitiveType.Quad);
		go.transform.SetParent(mapTrans, false);

		Vector2 endToEnd = mapData.poitsus[connection.end2Id].worldPos - mapData.poitsus[connection.end1Id].worldPos;
		Vector2 midPos = mapData.poitsus[connection.end1Id].worldPos + endToEnd / 2f;

		//GameObject lineGo = GameObject.CreatePrimitive(PrimitiveType.Quad);
		GameObject lineGo = go;
		//lineGo.transform.SetParent(go.transform, false);
		lineGo.transform.localRotation = Quaternion.AngleAxis(Mathf.Atan2(endToEnd.y, endToEnd.x) * Mathf.Rad2Deg, new Vector3(0, 0, 1));
		lineGo.transform.localScale = new Vector3(endToEnd.magnitude * mapSize, 0.25f, 1f);

		/*
		GameObject iconGo = GameObject.CreatePrimitive(PrimitiveType.Quad);
		iconGo.transform.SetParent(go.transform, false);
		iconGo.transform.localPosition = Vector3.zero;
		*/

		go.transform.localPosition = new Vector3(midPos.x, midPos.y, 0) * mapSize;
		//go.transform.localRotation = Quaternion.identity;

		go.GetComponent<MeshRenderer>().material = material;
	}

	public static void addPoitsuNetworkToMap(Transform parentTrans, float mapSize, MapData mapData, Material poitsuMaterial, Material poitsuConnectionMaterial)
	{
		for (int i = 0; i < mapData.poitsus.Count; ++i)
		{
			addPoitsuToMap(mapData.poitsus[i], parentTrans, mapSize, poitsuMaterial);
		}

		for (int i = 0; i < mapData.connections.Count; ++i)
		{
			addConnectionToMap(mapData.connections[i], mapData, parentTrans, mapSize, poitsuConnectionMaterial);
		}
	}

	public static int getOtherPoitsuId(PoitsuConnection connection, int currentPoitsuId)
	{
		return connection.end1Id == currentPoitsuId ? connection.end2Id : connection.end1Id;
	}
}
