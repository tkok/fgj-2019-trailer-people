﻿using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine.SceneManagement;

public enum MapPhase
{
	idle,
	arrive,
	selectDestination,
	leave
}

struct Progress
{
	public float targetFloodProgress;
	public float floodProgress;
	public int currentPoitsu;
	public List<int> visited;
}

public class MapManager : MonoBehaviour
{
	public MapData mapData;
	public Transform mapRootTrans;
	public Transform cameraTrans;
	public MeshRenderer mapMeshRenderer;
	public Transform youAreHereTrans;

	public float mapSize = 100f;

	public Vector2 camPosMin;
	public Vector2 camPosMax;
	public AnimationCurve camPosXCurve;
	public AnimationCurve camPosYCurve;

	public AnimationCurve floodCurve;

	public Material poitsuMaterial;
	public Material poitsuConnectionMaterial;

	public MapPhase mapPhase { get; private set; }

	private Progress progress;

	// Start is called before the first frame update
	void Start()
	{
		DontDestroyOnLoad(gameObject);
		BlackBoard.mapManager = this;

		// Init progress
		progress.currentPoitsu = 0;
		progress.visited = new List<int>();
		progress.floodProgress = 0;

		mapPhase = MapPhase.idle;

		populateMap();

		gameObject.SetActive(false);
	}

	// Update is called once per frame
	void Update()
	{
		if (mapPhase == MapPhase.arrive)
		{
			float applyProgress = Time.deltaTime * 0.125f;
			progress.floodProgress = progress.floodProgress + applyProgress * (progress.floodProgress < progress.targetFloodProgress ? 1f : -1f);

			if (Mathf.Abs(progress.targetFloodProgress - progress.floodProgress) < 0.001f)
			{
				if (getCurrentPoitsu().name == "Utsjoki")
				{
					SceneManager.LoadScene("you_win");
					BlackBoard.mapManager = null;
					Destroy(gameObject);
				}

				progress.floodProgress = progress.targetFloodProgress;
				mapPhase = MapPhase.selectDestination;
			}
		}

		updateMapVisuals(progress.floodProgress);
		updateCamPos(progress.floodProgress);
		updateYouAreaHere();
	}

	Material getMapMaterial()
	{
		List<Material> materials = new List<Material>();
		mapMeshRenderer.GetMaterials(materials);

		Debug.Assert(materials.Count == 1, "Only one material allowed");

		Material mapMat = materials[0];
		return mapMat;
	}

	public void finishedDrive()
	{
		mapPhase = MapPhase.arrive;

		Poitsu poitsu = getCurrentPoitsu();
		progress.targetFloodProgress = (poitsu.worldPos.y + 0.5f) / 1.0f;

		Debug.Log("Drive finished. Arrived at: " + poitsu.name);

		gameObject.SetActive(true);
	}

	public int getCurrentPoitsuId()
	{
		return progress.currentPoitsu;
	}

	public Poitsu getCurrentPoitsu()
	{
		return mapData.poitsus[progress.currentPoitsu];
	}

	public List<int> getConnectionsFromPoitsu(int poitsuId)
	{
		List<int> result = new List<int>();

		for (int i = 0; i < mapData.connections.Count; ++i)
		{
			if (mapData.connections[i].end1Id == poitsuId || mapData.connections[i].end2Id == poitsuId)
				result.Add(i);
		}

		return result;
	}

	public void selectNextConnection(int connectionId)
	{
		mapPhase = MapPhase.leave;
		progress.visited.Add(MapUtils.getOtherPoitsuId(mapData.connections[connectionId], getCurrentPoitsuId()));

		PoitsuConnection connection = mapData.connections[connectionId];
		int nextPoitsuId = MapUtils.getOtherPoitsuId(connection, getCurrentPoitsuId());
		progress.currentPoitsu = nextPoitsuId;

		Debug.Log("Next connection selected. ID: " + connectionId);

		BlackBoard.DriveGameManager.StartNextLevel(connection.driveLevelDataId);

		gameObject.SetActive(false);
	}

	public bool getFlooded(Vector2 pos)
	{
		Material mapMat = getMapMaterial();
		Texture2D tex = (Texture2D)mapMat.GetTexture("_MainTex");
		Color texel = tex.GetPixel(Mathf.RoundToInt((pos.x + 0.5f) * tex.width), Mathf.RoundToInt((pos.y + 0.5f) * tex.height));

		return floodCurve.Evaluate(progress.floodProgress) > texel.r;
	}

	void updateYouAreaHere()
	{
		Poitsu currentPoitsu = getCurrentPoitsu();
		Vector2 posOnMap = currentPoitsu.worldPos * mapSize;

		youAreHereTrans.localPosition = new Vector3(posOnMap.x, 2, posOnMap.y);
	}

	void updateMapVisuals(float t)
	{
		Material mapMat = getMapMaterial();
		mapMat.SetFloat("_WaterHeight", floodCurve.Evaluate(t));
	}

	void updateCamPos(float t)
	{
		cameraTrans.localPosition = new Vector3
		(
			camPosMin.x + (camPosMax.x - camPosMin.x) * camPosXCurve.Evaluate(t),
			cameraTrans.localPosition.y,
			camPosMin.y + (camPosMax.y - camPosMin.y) * camPosYCurve.Evaluate(t)
		);
	}

	void populateMap()
	{
		MapUtils.addPoitsuNetworkToMap(mapRootTrans, mapSize, mapData, poitsuMaterial, poitsuConnectionMaterial);
	}
}
