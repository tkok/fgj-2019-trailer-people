﻿using System.Collections.Generic;
using UnityEngine;

public class DestinationSelector : MonoBehaviour
{
	public Transform indicatorTrans;

	private MapManager mapManager;

	void Start()
	{
		mapManager = GetComponent<MapManager>();
	}

	void OnGUI()
	{
		if (!mapManager)
		{
			Debug.LogError("MapManager not found");
			return;
		}

		if (mapManager.mapPhase != MapPhase.selectDestination)
			return;

		int currentPoitsuId = mapManager.getCurrentPoitsuId();
		List<int> connections = mapManager.getConnectionsFromPoitsu(currentPoitsuId);

		if (connections.Count == 0)
		{
			Debug.LogError("No connnections from this poitsu! How did you even get here?");
			return;
		}

		float scale = 500f;
		Vector2 buttonSize = new Vector2(1f / connections.Count, 0.15f) * scale;

		for (int i = 0; i < connections.Count; ++i)
		{
			PoitsuConnection connection = mapManager.mapData.connections[connections[i]];

			int destinationPoitsuId = MapUtils.getOtherPoitsuId(connection, currentPoitsuId);
			Poitsu destinationPoitsu = mapManager.mapData.poitsus[destinationPoitsuId];

			bool flooded = mapManager.getFlooded(destinationPoitsu.worldPos);
			string text = destinationPoitsu.name;
			/*if (flooded)
			{
				text = text + " (FLOODED!)";
			}*/

			Rect buttonRect = new Rect(new Vector2(i * buttonSize.x, 0), buttonSize);
			if (GUI.Button(buttonRect, text))
			{
				mapManager.selectNextConnection(connections[i]);
			}
		}
	}
}
