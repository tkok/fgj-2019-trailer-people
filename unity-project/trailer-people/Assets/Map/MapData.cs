﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct PoitsuConnection
{
	public int end1Id;
	public int end2Id;
	public int driveLevelDataId;
}

[System.Serializable]
public struct Poitsu
{
	public string name;
	public Vector2 worldPos;
}

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Map Data", order = 1)]
public class MapData : ScriptableObject
{
	public List<Poitsu> poitsus;
	public List<PoitsuConnection> connections;
}
